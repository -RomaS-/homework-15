package stolbunov.roman.homework_15.data.fighters;

public interface PostAttackAction {
    void postAttackAction (float damageTaken, float damageGotten);
}
