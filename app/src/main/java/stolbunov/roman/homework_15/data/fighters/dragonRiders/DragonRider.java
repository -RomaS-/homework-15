package stolbunov.roman.homework_15.data.fighters.dragonRiders;


import android.os.Parcel;
import android.util.Log;

import stolbunov.roman.homework_15.data.fighters.ArenaFighter;
import stolbunov.roman.homework_15.data.fighters.FighterType;
import stolbunov.roman.homework_15.data.fighters.dragons.Dragon;

public class DragonRider extends ArenaFighter {
    private Dragon ridingDragon;
    private boolean flagDragon;

    public DragonRider(String name, float health, float damage, float armor, String url) {
        super(name, health, damage, armor, url);
        classFighter = FighterType.DRAGON_RIDER.name();
    }

    public DragonRider(Parcel in) {
        super(in);
        this.ridingDragon = in.readParcelable(Dragon.class.getClassLoader());
    }

    @Override
    public float attack(ArenaFighter var1) {
        if (var1 instanceof Dragon && !flagDragon) {
            attackDragon((Dragon) var1);
            return 0;
        } else {
            return var1.damaged(this.damage);
        }
    }

    private void attackDragon(Dragon dragon) {
        flagDragon = true;
        this.ridingDragon = dragon;
        health += dragon.getHealth();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeParcelable(ridingDragon, flags);
    }

    @Override
    protected int getType() {
        return FighterType.DRAGON_RIDER.getType();
    }
}
