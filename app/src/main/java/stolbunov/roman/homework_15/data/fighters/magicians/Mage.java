package stolbunov.roman.homework_15.data.fighters.magicians;


import stolbunov.roman.homework_15.data.fighters.ArenaFighter;
import stolbunov.roman.homework_15.data.fighters.Elements;

public class Mage extends ArenaFighter implements Elements {
    int element;

    public Mage (String name, float health, float damage, float armor, int element) {
        super(name, health, damage, armor,null);
        this.element = element;
    }

    @Override
    public float attack (ArenaFighter fighters) {
        if( fighters instanceof Elements ) {
            if( isElementsEquals(((Elements) fighters).getElements()) ) {
                return 0;
            }
        }
        return fighters.damaged(damage);
    }

    @Override
    protected int getType() {
        // todo create type for fighter
        return 0;
    }

    @Override
    public int getElements () {
        return element;
    }
}
