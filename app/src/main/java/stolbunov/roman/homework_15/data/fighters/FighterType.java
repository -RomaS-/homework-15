package stolbunov.roman.homework_15.data.fighters;

public enum FighterType {
    DRAGON(1), DRAGON_RIDER(2), KNIGHT(3);

    int type;

    FighterType (int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public static FighterType randomType() {
        int numEnemy = (int) (Math.random() * 3) + 1;
        switch (numEnemy) {
            case 1:
                return DRAGON;
            case 2:
                return DRAGON_RIDER;
            case 3:
                return KNIGHT;
        }
        return DRAGON;
    }
}
