package stolbunov.roman.homework_15.data.fighters;

public interface Elements {
    int FIRE = 2;
    int WATER = 4;
    int EARTH = 8;
    int WIND = 16;

    int getElements ();

    default boolean isElementsEquals (int elements) {
        return (getElements() & elements) == getElements();
    }
}
