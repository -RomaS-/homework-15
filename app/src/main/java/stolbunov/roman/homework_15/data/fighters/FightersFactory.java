package stolbunov.roman.homework_15.data.fighters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import stolbunov.roman.homework_15.data.fighters.dragonRiders.DragonRider;
import stolbunov.roman.homework_15.data.fighters.dragons.Dragon;
import stolbunov.roman.homework_15.data.fighters.knights.Knight;

public class FightersFactory {
    static private int counterNames = 0;

    static HashMap<FighterType, String> urlDataBase = new HashMap<>();
    static List<String> listNames = new ArrayList<>();

    static {
        urlDataBase.put(FighterType.DRAGON, "http://wm.schoolofdragons.com/SoD/Joomla/templates/schoolofdragons/images/crafts/dragon-cut-out.jpg");
        urlDataBase.put(FighterType.DRAGON_RIDER, "http://static.tvtropes.org/pmwiki/pub/images/FireEmblemHeath_2008.jpg");
        urlDataBase.put(FighterType.KNIGHT, "https://thumbs.dreamstime.com/b/%D1%81%D0%BC%D0%B5%D1%88%D0%BD%D0%BE%D0%B9-%D1%80%D1%8B%D1%86%D0%B0%D1%80%D1%8C-67339345.jpg");

        listNames.add("Arthur");
        listNames.add("Leonard");
        listNames.add("Sheldon");
        listNames.add("Jack");
        listNames.add("Carl");
        listNames.add("Harry");
        listNames.add("Gregory");
        listNames.add("Richard");
    }

    static class UnknownFighterException extends RuntimeException {
    }

    public static ArenaFighter generateFighter(FighterType fighterType) {
        return generateFighter(fighterType, generateName());
    }

    public static ArenaFighter generateFighter(FighterType fighterType, String name) {
        switch (fighterType) {
            case DRAGON:
                return new Dragon(name, generateValue(300),
                        generateValue(30),
                        generatePercentValue(), 0,
                        urlDataBase.get(fighterType));
            case KNIGHT:
                return new Knight(name, generateValue(300),
                        generateValue(30),
                        generatePercentValue(),
                        generatePercentValue(),
                        urlDataBase.get(fighterType)
                );
            case DRAGON_RIDER:
                return new DragonRider(name,
                        generateValue(300),
                        generateValue(30),
                        generatePercentValue(),
                        urlDataBase.get(fighterType));

        }
        throw new UnknownFighterException();
    }

    private static float generateValue(int max) {
        Random random = new Random();
        return Math.abs(random.nextInt(max));

    }

    private static float generatePercentValue() {
        Random random = new Random();
        return (float) Math.abs(random.nextGaussian());
    }

    private static String generateName() {
        String name;
        if (counterNames < listNames.size()) {
            name = listNames.get(counterNames);
        } else {
            counterNames = 0;
            name = listNames.get(counterNames);
        }
        counterNames++;
        return name;
    }
}
