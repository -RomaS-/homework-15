package stolbunov.roman.homework_15.data.fighters.knights;


import android.os.Parcel;

import stolbunov.roman.homework_15.data.fighters.PostAttackAction;

public class HollyKnight extends Knight implements PostAttackAction {
    private float recovery;

    public HollyKnight(String name, float health, float damage, float armor, float shield, float recovery) {
        super(name, health, damage, armor, shield, null);
        this.recovery = recovery;
    }

    public HollyKnight(Parcel in) {
        super(in);
        recovery = in.readFloat();
    }

    private void recovery() {
        if (this.isAlfie()) {
            this.heal(recovery);
            System.out.println(this.getName() + " recovery " + recovery);
        }
    }

    @Override
    public void postAttackAction(float damageTaken, float damageGotten) {
        recovery();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeFloat(recovery);
    }

    @Override
    protected int getType() {
        //// todo create type for fighter
        return 0;
    }
}