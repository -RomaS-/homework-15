package stolbunov.roman.homework_15.ui.screens;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import stolbunov.roman.homework_15.R;
import stolbunov.roman.homework_15.bl.arenas.BattleArena;
import stolbunov.roman.homework_15.bl.arenas.DuelArenaFactory;
import stolbunov.roman.homework_15.data.fighters.ArenaFighter;
import stolbunov.roman.homework_15.data.fighters.FighterType;
import stolbunov.roman.homework_15.data.fighters.FightersFactory;

public class MainActivity extends AppCompatActivity {

    private static final String CURRENT_FIGHTER_KEY = "CURRENT_FIGHTER_KEY";
    private static final String URI_VS_IMAGE =
            "http://static1.comicvine.com/uploads/original/11112/111129141/5440487-1122329314-52705.png";
    private float startHPFighter;
    private float startHPEnemy;

    private ImageView profileImage;

    private TextView profileName;
    private TextView profileDescription;

    private TextView currentHp;
    private TextView currentArmor;
    private TextView currentDamage;

    private TextView startNewChange;

    private LinearLayout battleHistory;
    private ImageView battleImageFighter1;
    private ImageView battleImageFighter2;
    private ImageView battleImageVS;
    private TextView battleNameFighter1;
    private TextView battleNameFighter2;
    private TextView battleHPFighter1;
    private TextView battleHPFighter2;
    private TextView battleResult;
    private View battleView;

    private ArenaFighter currentFighter;
    private ArenaFighter currentEnemy;
    private ArenaFighter currentWinner;

    private BattleArena arena;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        initFighter(savedInstanceState);
        setDataOnUi();

    }

    private void setDataOnUi() {
        profileName.setText(currentFighter.getName());
        profileDescription.setText(currentFighter.getDescription());

        loadingImage(currentFighter, profileImage);

        currentHp.setText(String.valueOf(currentFighter.getHealth()));
        currentDamage.setText(String.valueOf(currentFighter.getDamage()));
        currentArmor.setText(String.valueOf(currentFighter.getArmor()));


    }

    private void initFighter(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            currentFighter = FightersFactory.generateFighter(FighterType.DRAGON_RIDER, "Ikking");
        } else {
            currentFighter = savedInstanceState.getParcelable(CURRENT_FIGHTER_KEY);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(CURRENT_FIGHTER_KEY, currentFighter);
    }

    private void startNewFight(View view) {
        if (currentFighter.getHealth() > 0) {
            currentEnemy = FightersFactory.generateFighter(FighterType.randomType());
            startHPFighter = currentFighter.getHealth();
            startHPEnemy = currentEnemy.getHealth();
            arena = DuelArenaFactory.create(currentFighter, currentEnemy);
            arena.startBattle();
            currentWinner = arena.getWinner();
            addFightResultOnLayout();
        } else {
            Toast.makeText(this, R.string.fighter_died, Toast.LENGTH_SHORT).show();
        }

    }

    private void addFightResultOnLayout() {
        initViewForInflate();
        fillingView();
        battleHistory.addView(battleView);
    }

    private void fillingView() {
        loadingImage(currentFighter, battleImageFighter1);
        loadingImage(currentEnemy, battleImageFighter2);
        loadingImage(URI_VS_IMAGE, battleImageVS);
        battleNameFighter1.setText(currentFighter.getName());
        battleNameFighter2.setText(currentEnemy.getName());
        battleHPFighter1.setText(String.valueOf(startHPFighter));
        battleHPFighter2.setText(String.valueOf(startHPEnemy));
        battleResult.setText(String.format(
                getResources().getString(R.string.battle_result),
                currentWinner.getName(),
                currentWinner.getTotalDamage(),
                currentWinner.getHealth()));
    }

    private void loadingImage(ArenaFighter fighter, ImageView view) {
        loadingImage(fighter.getImageUrl(), view);
    }


    private void loadingImage(String uri, ImageView view) {
        Glide.with(this)
                .load(uri)
                .into(view);
    }

    private void initViews() {
        profileImage = findViewById(R.id.fighter_icon);

        profileName = findViewById(R.id.fighter_name);
        profileDescription = findViewById(R.id.fighter_description);

        currentHp = findViewById(R.id.value_hp);
        currentArmor = findViewById(R.id.value_armor);
        currentDamage = findViewById(R.id.value_damage);

        startNewChange = findViewById(R.id.btn_start_new_chalange);
        startNewChange.setOnClickListener(this::startNewFight);

        battleHistory = findViewById(R.id.battle_history);
    }

    private void initViewForInflate() {
        LayoutInflater inflater = getLayoutInflater();
        battleView = inflater.inflate(R.layout.show_battle_fighters, battleHistory, false);
        battleImageFighter1 = battleView.findViewById(R.id.iv_battle_fighter1);
        battleImageFighter2 = battleView.findViewById(R.id.iv_battle_fighter2);
        battleImageVS = battleView.findViewById(R.id.iv_battle_vs);
        battleNameFighter1 = battleView.findViewById(R.id.tv_name_fighter1);
        battleNameFighter2 = battleView.findViewById(R.id.tv_name_fighter2);
        battleHPFighter1 = battleView.findViewById(R.id.tv_hp_fighter1);
        battleHPFighter2 = battleView.findViewById(R.id.tv_hp_fighter2);
        battleResult = battleView.findViewById(R.id.tv_battle_result);
    }
}
