package stolbunov.roman.homework_15.bl.arenas;


import stolbunov.roman.homework_15.data.fighters.ArenaFighter;
import stolbunov.roman.homework_15.data.healers.Healer;

public class DuelArena extends BattleArena {
    protected ArenaFighter participant1;
    protected ArenaFighter participant2;
    private int roundCountMax;


    public DuelArena(Healer healer, ArenaFighter participant1, ArenaFighter participant2, int roundCountMax) {
        super(healer);
        this.participant1 = participant1;
        this.participant2 = participant2;
        this.roundCountMax = roundCountMax;
        resetDamageCounter(participant1);
        resetDamageCounter(participant2);
    }

    @Override
    public void startBattle() {
        int currentRound = 0;
        while (currentRound < roundCountMax && isFightContinue(participant1, participant2)) {
            confrontation(participant1, participant2);
            currentRound++;
        }
    }

    @Override
    public ArenaFighter getWinner() {
        ArenaFighter winners = calculationOfWinner(participant1, participant2);
        if (winners != null) {
            return winners;
        } else {
            return null;
        }
    }

    private void resetDamageCounter(ArenaFighter fighter) {
        fighter.setTotalDamage(0);
    }
}
