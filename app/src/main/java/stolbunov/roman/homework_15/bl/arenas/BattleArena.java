package stolbunov.roman.homework_15.bl.arenas;


import stolbunov.roman.homework_15.data.fighters.ArenaFighter;
import stolbunov.roman.homework_15.data.fighters.PostAttackAction;
import stolbunov.roman.homework_15.data.healers.Healer;

public abstract class BattleArena {
    protected Healer healer;
    protected GodHand godHand;

    public BattleArena(Healer healer) {
        this.healer = healer;
    }

    public abstract void startBattle();

    public void setGodHand(GodHand godHand) {
        this.godHand = godHand;
    }

    public abstract ArenaFighter getWinner();

    public ArenaFighter calculationOfWinner(ArenaFighter participant1, ArenaFighter participant2) {
        if (participant1 != null && participant2 != null) {
            if (participant1.isAlfie() && participant2.isAlfie()) {
                return (participant1.getHealth() > participant2.getHealth()) ? participant1 : participant2;
            } else if (participant1.isAlfie()) {
                return participant1;
            } else if (participant2.isAlfie()) {
                return participant2;
            }
        }
        return null;
    }

    protected void confrontation(ArenaFighter participant1, ArenaFighter participant2) {
        float dam1 = participant1.attack(participant2);
        float dam2 = participant2.attack(participant1);
        if (participant1 instanceof PostAttackAction) {
            ((PostAttackAction) participant1).postAttackAction(dam1, dam2);
        }

        if (participant2 instanceof PostAttackAction) {
            ((PostAttackAction) participant2).postAttackAction(dam2, dam1);
        }

        if (healer != null) {
            healer.heal(dropTheCoin() ? participant1 : participant2);
        }

        if (godHand != null) {
            godHand.godHand(participant1, participant2);
        }

    }

    protected boolean dropTheCoin() {
        int randomNum = (int) (Math.random() * 100);
        return (randomNum % 2) == 0;
    }

    protected boolean isFightContinue(ArenaFighter participant1, ArenaFighter participant2) {
        return participant1 != null && participant2 != null && participant1.isAlfie() && participant2.isAlfie();
    }

    public interface GodHand {
        boolean godHand(ArenaFighter participant1, ArenaFighter participant2);
    }
}
