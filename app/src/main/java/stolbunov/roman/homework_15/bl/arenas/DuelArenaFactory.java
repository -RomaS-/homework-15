package stolbunov.roman.homework_15.bl.arenas;

import stolbunov.roman.homework_15.data.fighters.ArenaFighter;
import stolbunov.roman.homework_15.data.healers.Healer;

public class DuelArenaFactory {
    public static BattleArena create(ArenaFighter fighter1, ArenaFighter fighter2) {
        return new DuelArena(null, fighter1, fighter2, 5);
    }
}
